import { Injectable } from '@angular/core';
import { Image } from './image.model';

@Injectable()
export class ImageService {
  private _images: Image[] = [];
  selectedImage: Image;
  constructor() { }

  get images() {
    return this._images;
  }
  set images(images: Image[]) {
    this._images = images;
  }

  selectImage(image: Image): void {
    this.selectedImage = image;
  }


}
