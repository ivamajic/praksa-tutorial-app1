import { Component, OnInit, Input } from '@angular/core';
import { Image } from '../../image.model';
import { ImageService } from '../../image.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  @Input() public image: Image;

  constructor(private _image: ImageService) { }

  ngOnInit() {
  }

}
