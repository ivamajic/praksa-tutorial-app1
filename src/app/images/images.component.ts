import { ImageService } from './../image.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Image } from '../image.model';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent implements OnInit {

  @Input() public image: Image;
  @Output() public onSelect: EventEmitter<any> = new EventEmitter;
  @Output() public onEdit: EventEmitter<Image> = new EventEmitter;

  public images: Image[] = [];
  constructor(
    private _image: ImageService
  ) { }

  ngOnInit() {
    for (let i = 0; i < 4; i++) {
      let img = new Image("https://images.pexels.com/photos/1023953/pexels-photo-1023953.jpeg?auto=compress&cs=tinysrgb&h=350", "image " + i, "Generic description " + i);
      this.images.push(img);
    }
  }

  selectImage(image: Image): void {
    this._image.selectImage(image);
    this.onSelect.emit(true);
  }


  editImage(image: Image): void {
    this._image.selectImage(image);
    this.onEdit.emit(image);

  }

}
