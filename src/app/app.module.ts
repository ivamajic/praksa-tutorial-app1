import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { ImagesComponent } from './images/images.component';
import { DetailsComponent } from './images/details/details.component';
import { EditComponent } from './edit/edit.component';
import { ImageService } from './image.service';


@NgModule({
  declarations: [
    AppComponent,
    ImagesComponent,
    DetailsComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [ImageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
