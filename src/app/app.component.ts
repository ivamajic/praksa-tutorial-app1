import { Component } from '@angular/core';
import { ImageService } from './image.service';
import { Image } from './image.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  tab: string = "images";
  imageForEdit: Image;

  constructor(public _image: ImageService) {

  }

  changeTab(tab: string): void {
    this.tab = tab;
  }


}
